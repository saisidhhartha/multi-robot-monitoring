import rospy
import socketio
from std_msgs.msg import String, Bool, Int32
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
from std_msgs.msg import String, Bool, Int32
import math
from geometry_msgs.msg import Point, PoseStamped, Pose, Quaternion
#from tf import transformations
#from tf.transformations import quaternion_from_euler, euler_from_quaternion
#import thread
import time
from slack import WebClient
from slack.errors import SlackApiError
from scipy.spatial import distance
import numpy as np

rospy.init_node('visualizer')
point_viz_1 = rospy.Publisher('point_viz_1',PoseStamped,queue_size=1)
point_viz_2 = rospy.Publisher('point_viz_2',PoseStamped,queue_size=1)



slack_token = 'xoxb-613997337333-1280245591442-n2AZ43gwvuWRVRPhtFBEb6py'
client = WebClient(token=slack_token)
print('[INFO] Esablishing socket connection')
sio = socketio.Client()
server_url = "https://sockets.mitrarobot.com" 
sio.connect(server_url)
print('[INFO] Esablished socket connection')


x1,y1,x2,y2 = None,None,None,None

while True:    
    def euler_to_quaternion(yaw, pitch, roll):
        qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
        qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
        qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        return qx, qy, qz, qw

    #convert points from socket to ROS compatable format (PoseStamped)
    def xya2ps(x,y,angle):
        pos_x = x
        pos_y = y
        pos_angle = angle

        pos_angle = math.radians(pos_angle)  # angle in rad
        p = PoseStamped()
        q = Quaternion()
        q.x, q.y, q.z, q.w = euler_to_quaternion(0, 0, pos_angle)
        p.pose.position.x = pos_x
        p.pose.position.y = pos_y
        p.pose.position.z = 0
        p.pose.orientation = q       
        p.header.frame_id = "map"
        now = rospy.get_rostime()
        p.header.seq = 0
        p.header.stamp = now
        return p

    #Display Points on RVIZ
    def visualize_point(x,y,a,robot):
        global x1,y1,x2,y2
        p = xya2ps(x,y,a)
        if robot=='itc1':
            print('Plotting ITC1')
            x1,y1 = x,y
            point_viz_1.publish(p)
        elif robot=='itc2':
            print('Plotting ITC2')
            x2,y2 = x,y
            point_viz_2.publish(p)
      
        print(x1,y1,x2,y2)
        if x1 and x2 and y1 and y2:
            print(distance.euclidean([x1,y1],[x2,y2]))
            if distance.euclidean([x1,y1],[x2,y2])<1:
                response = client.chat_postMessage(channel="ros_slack",text="Crash Alerts : Robots ITC 1,ITC 2")
            else:
                pass
        


            
    #Collect Points on RVIZ
    @sio.event
    def ros_loc(data):
        #{"robot_a":0,"robot_name":"itc1","robot_x":1,"robot_y":1,"robot_location":"ITC"}    
        x = float(data['robot_x'])
        y = float(data['robot_y'])
        a = float(data['robot_a'])
        robot_name = str(data['robot_name'])
        visualize_point(x,y,a,robot_name)

