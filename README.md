## Multi-Robot Monitoring Using Sockets and RVIZ

With a fleet of robots being deployed over various client locations. It eventually becomes important to see where the robots are moving around and their paths. This can help in various ways such as understanding if the robots are collinding and also understand the most frequent trips. Various tools, such as freedom exist to monitor robots. But currently we are limited to viewing each robot as individually. This would mean opening multiple freedom windows at trying to intutively understnad their relative positons. This is tiring and the slow loading of maps and slow update speed of freedom adds to the trouble. We solve this problem, by using sockets to communicate locations over the internet from multiple robots. The locations are collected and visualized on RVIZ, which is easier to interpret and quicker to understand. 

## Sockets 
Sockets are simply used as a channel. Recommend Socket Library Python3's socketio. This has been found to be reliable and not break the connection. To establish the communication, the following are required.

1. Place the socket_share.py program in the NUC. Make sure you edit the robotname, robot location in the sio.emit() function.

2. Run the program python3 socket_share.py , You can move the robot around and see the robot pushing the /amcl_topic messages over sockets.

## RVIZ
1. In your local system, run python3 socket_collect.py . This should catch hold of the robot locations. Edit the code as per the number of robots and robot names. Currently the code is suited for 2 or 1 robot named 'itc1' or 'itc2'

2. Run your rviz and add the topics. (Make sure you have your map server running)


### Commands

If the instructions are not found to be clear, run the follwing commands in the correct order.

1. python3 socket_share.py  (in robot 1)
2. python3 socket_share_2.py  (in robot 2, if exists)
3. python3 socket_collect.py (in local system)
4. rosrun map_server map_server <you_map_name>.yaml
5. rosrun rviz rviz

Finally add the topics you want to visualize in rviz. Here add, the following 
1. map
2. Pose, topic : point_viz_2
3. Pose, topic : point_viz_1
