import rospy
import socketio
from std_msgs.msg import String, Bool, Int32
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseStamped
from std_msgs.msg import String, Bool, Int32
import math
from geometry_msgs.msg import Point, PoseStamped, Pose, Quaternion
import time

sio = None
count = 0 
print('[INFO] Esablishing socket connection')
sio = socketio.Client()
server_url = "https://sockets.mitrarobot.com" 
sio.connect(server_url)
print('[INFO] Esablished socket connection')

def connect_socket():
    global sio
    print('[INFO] Esablishing socket connection')
    sio = socketio.Client()
    server_url = "https://sockets.mitrarobot.com" 
    sio.connect(server_url)
    print('[INFO] Esablished socket connection')
    

def euler_from_quaternion(x, y, z, w):
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    X = math.degrees(math.atan2(t0, t1))

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    Y = math.degrees(math.asin(t2))

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    Z = math.degrees(math.atan2(t3, t4))

    return [X, Y, Z]

#Convertor
def quaternion_to_euler(pose):
    euler = euler_from_quaternion(pose.orientation.x, pose.orientation.y,pose.orientation.z, pose.orientation.w)
    x = pose.position.x
    y = pose.position.y
    a = euler[2]*180/math.pi % 360
    return x,y,a

def get_curret_pose(data):
    global sio,count
    if count % 100==0:
        connect_socket()
    #print(data)
    x,y,a = quaternion_to_euler(data.pose.pose)
    print(x,y,a)

    try:
        sio.emit('ros_location', {'robot_x': x,'robot_y':y,'robot_a':a,'robot_location':'ITC','robot_name':'itc2'})
    except:
        print('Lost Socket Connection!')
        connect_socket()
        sio.emit('ros_location', {'robot_x': x,'robot_y':y,'robot_a':a,'robot_location':'ITC','robot_name':'itc2'})
        


    print('[INFO] Data sent to Socket',count)#collecting enxt point in 5 seconds')
    count = count+1
    #time.sleep(5)
    

def main():
    rospy.init_node('socket_ros', anonymous=True)
    rospy.Subscriber('amcl_pose',PoseWithCovarianceStamped, callback=get_curret_pose, queue_size=1)
    rospy.spin()

if __name__ == '__main__':
    main()

